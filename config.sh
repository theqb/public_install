#!/usr/bin/env bash

# Store script path
SCRIPT_PATH=$(pwd)

echo ///////////////////////////////////////////////////////////////////////////
read -p "Do you want to upgrade your linux? (y/n): " response
response=${response,,}
if [[ "$response" =~ ^(yes|y)$ ]]
then
echo We are now doing upgrades
sudo apt update
sudo apt -y dist-upgrade
sudo apt -y upgrade
fi

echo ///////////////////////////////////////////////////////////////////////////
read -p "Do you want to install keybase and basic packages? (y/n): " response
response=${response,,}   
if [[ "$response" =~ ^(yes|y)$ ]]
then
sudo apt update
sudo apt -y install git vim
sudo rm -rf /tmp/conf
mkdir /tmp/conf
cd /tmp/conf
curl -o /tmp/conf/keybase.deb https://prerelease.keybase.io/keybase_amd64.deb
sudo dpkg -i keybase.deb
sudo apt --fix-broken install
sudo apt-get install -f
sudo apt update
cd $SCRIPT_PATH
fi

